<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Licencia extends Model
{
    protected $fillable=['tipo', 'valor'];

    public function buses()
    {
        return $this->hasMany('App\Bus');
    }
}
