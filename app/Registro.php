<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    protected $fillable=['longitud', 'latitud', 'usuario_id'];

    public function ruta()
    {
        return $this->belongsTo('App\Ruta');
    }
}
