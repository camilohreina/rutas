<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
	protected $table='buses';

    protected $fillable = ['placa', 'capacidad', 'cilindraje', 'foto', 'licencia_estado', 'operacion_estado', 'licencia_id', 'fecha_expiracion', 'empresa_id'];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function rutas()
    {
        return $this->belongsToMany('App\Ruta');
    }

    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    public function licencia()
    {
        return $this->belongsTo('App\Licencia');
    }
}
