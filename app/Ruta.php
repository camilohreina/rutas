<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruta extends Model
{
    protected $fillable=['nombre', 'origen', 'destino', 'empresa_id', 'latitud_origen', 'longitud_origen', 'latitud_destino', 'ruta_url', 'longitud_destino'];

     public function buses()
    {
        return $this->belongsToMany('App\Bus');
    }

     public function registros()
    {
        return $this->hasMany('App\Registro');
    }

     public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }
}
