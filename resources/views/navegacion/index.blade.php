@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Navegacion')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Navegar</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="autobuses">
          <div class="card-title">
            <h2>Rutas en tiempo real</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection