@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Licencias')])
@section('estilos')
  <style>
    .label-area{
      width: 80px;
      height: 80px;
      position: absolute;
      top: -1px;
      right: -1px;
      text-align: center;
      border-top-right-radius: 3px;
      background: #ef4368;
    }
    .label-area span{
      position: absolute;
      top: 22px;
      right: 3px;
      text-transform: uppercase;
      transform: rotate(45deg);
      font-weight: 700;
      font-size: 10px;
      color: #fff;
      letter-spacing: 1px;
    }
    .label-area:before{
      content: '';
      display: block;
      width: 0;
      height: 0;
      border-style: solid;
      border-width: 80px 0 0 80px;
      border-color: transparent transparent transparent #fff;
    }
  </style>
@endsection
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Licencias</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="licencias">
          <div class="card-title">
            <div class="container">
              <div class="card-deck mb-3 text-center">
                @foreach ($rows as $row)
                <div class="card mb-4 shadow-sm">
                  <div class="card-header">
                    <h4 class="my-0 font-weight-normal">{{ucfirst($row->tipo)}}</h4>
                    <hr>
                  </div>
                  <div class="card-body">
                    <h2 class="card-title pricing-card-title">{{$row->valor}} COP </h2>
                    <h5 class="text-muted">POR BUS</h5>
                    <ul class="list-unstyled mt-3 mb-4">
                      <li>Localizacion en tiempo real</li>
                      <li>Soporte directo</li>
                      <li>Control de rutas </li>
                      <li>Planificacion de rutas</li>
                      <li>Acceso a la App movil</li>
                    </ul>
                  <a href="{{url('licencias/'.$row->id.'/edit')}}" class="btn  btn-block btn-info">EDITAR</a>
                  </div>
                </div>
              @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection