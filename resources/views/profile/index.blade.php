@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Typography')])
@section('estilos')
    <style>
      #imageUploadInput {
        display: none;
      }
      .image {
        max-width: 300px;
        height: auto;
        margin-bottom: 0 2px 3px rgba(0, 0, 0, 0.4);
      }
      .image-name {
        margin: 16px 0;
        text-align: center;
        display: none;
      }
      .button {
        color: white;
        border: 0;
        border-radius: 0.2rem;
        padding: 7px 12px;
        box-shadow: 0 2px 3px rgba(0, 0, 0, 0.4);
        font-size: 14px;
        font-weight: 400;
        position: relative;
        transition: .2s ease-in-out;
        outline: none;
        }
        .button:hover {
          box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.6);
          cursor: pointer;
        }
        .choose-image-button {
          background: #919191;
          margin-right: 8px;
        }
        .upload-button {
          background: #47A44B;
        }
    </style>
@endsection
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Perfil</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="autobuses">
          <div class="card-title">
            <div class="row ">
                <div class="col-md-4">
                @if (isset($data->foto))
                    <img src="{{ url('public/images/'.$data->foto)  }}" class="w-100"> 
                @else            
                    <img src="http://placehold.it/350x350" class="w-100">
                @endif
                @if ($data->id==Auth::id())
                <form action="{{url('cambiar-foto')}}" class="form" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="_method" value="PATCH">
                  @csrf
                    <div class="mt-2 ml-5">
                      <label 
                        for="imageUploadInput"
                        class="button choose-image-button">
                        Cambiar imagen
                        <input type="file" name="foto" id="imageUploadInput">
                      </label>
                      <button type="submit" class="btn btn-success btn-sm ">
                        <i class="material-icons">publish</i>
                      </button>
                    </div>
                    {{-- <p class="image-name">Image name</p> --}}
                  </form>
                @endif
                
                  </div>
                  <div class="col-md-8 px-3">
                    <div class="card-block px-3">
                      <h4 class="card-title"> {{$data->nombre}} {{$data->apellido}}</h4>
                      <p class="card-text">Email: {{$data->email}}</p>
                      <p class="card-text">Cargo: {{$data->role->nombre_visible}} </p>
                      <p class="card-text">Empresa: {{$data->empresa->nombre}}</p>
                      @if (Auth::user()->role_id==2)      
                      <a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">Cambiar Role</a>
                      @endif
                    </div>
                  </div>
        
                </div>
          </div>
          <div class="card-footer  ml-2 mr-2">
            <a href=" {{url('empleados')}} " class="btn btn-info pull-right">Volver</a>
            @if (Auth::id() == $data->id)   
            <a href="#" class="btn btn-primary float-right">Editar perfil</a>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cambiar role</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow: hidden;">
        <form action=" {{url('cambiar-empresa')}} " method="post" class="d-inline" autocomplete="off">
        <input type="hidden" name="_method" value="PATCH">
        @csrf
        <input type="hidden" name="id" value=" {{$data->id}} ">
        <div class="mb-2">
          <select class="form-control" name="role" width="100%" required>
            <option disabled selected>Seleccione el role para este usuario...</option>
            @foreach ($roles as $role)
              @if ($role->nombre_visible != 'Super Administrador')  
              <option value=" {{$role->id}} "> {{ ucfirst($role->nombre_visible)}} </option>
              @endif
            @endforeach
          </select>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Cambiar</button>
        </div>
      </form>
      </div>
  </div>
</div>
@endsection