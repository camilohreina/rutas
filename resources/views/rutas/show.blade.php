@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Typography')])

<style type="">
  
  iframe{
    width: 1000px;
    height: 700px;
  }
</style>

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Rutas</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="autobuses">
          <div class="card-title">
            <h2>{{$data->nombre}}</h2>
            <br>

            <center>
              
               {!!$data->ruta_url!!}

            </center>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection