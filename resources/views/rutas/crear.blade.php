@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Typography')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Rutas</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="autobuses">
          <div class="card-title">
            <h2>Crear Ruta</h2>
            <br>
            
            <form action="{{url('rutas')}}" method="post">
             @csrf
              <div class="form-group">
                <label for="nombre">Nombre</label>
                <input required type="text" class="form-control" id="nombre" name="nombre">
              </div>

              <br>

              <div class="form-group">
                <label for="origen">Origen</label>
                <input required type="text" class="form-control" id="origen" name="origen">
              </div>

              <br>

              <div class="form-group">
                <label for="destino">Destino</label>
                <input required type="text" class="form-control" id="destino" name="destino">
              </div>

              <br>

              
              @if(Auth::user()->role_id==1)

              <div class="form-group">
                <label for="empresa">Empresa</label>
                <select required class="form-control" data-style="btn btn-link" id="empresa" name="empresa_id">
                  <option value="" selected disabled>Seleccione...</option>

                  @foreach ($data as $row)
                  <option value="{{$row->id}}">{{$row->nombre}}</option>
                  @endforeach

                </select>
              </div>
              @endif


               <div class="form-group">
                <label for="destino">Crear Ruta:</label>
                <br>
                <a id="maps" href="#" class="btn btn-primary" target="_blank">Crear</a>
              </div>

              <br>

              <div class="form-group">
                <label for="destino">Código embebido</label>
                <input required type="text" class="form-control" id="destino" name="ruta_url">
              </div>
              
              <br>

              <button type="submit" class="btn btn-success">Submit</button>
              <a href="{{url('rutas')}}" class="btn btn-info float-right">Volver</a>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
<script type="text/javascript">
  

  $("#maps").click(function(e){

    // e.preventDefault();

  var origen = $('#origen').val();
  var destino = $('#destino').val();
  var url = 'https://www.google.com/maps/dir/'+origen+'/'+destino;

    $(this).attr('href', url );
  })
</script>
@endsection