@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Typography')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Autobuses</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="autobuses">
          <div class="card-title">
            <h2>Crear Bus</h2>
            <br>
            
            <form action="{{ url('autobuses') }}" method="post" enctype="multipart/form-data">
             @csrf
              <div class="form-group">
                <label for="placa">Placa</label>
                <input required type="text" class="form-control" id="placa" name="placa">
              </div>

              <br>

              <div class="form-group">
                <label for="capacidad">Capacidad de personas</label>
                <input required type="number" class="form-control" id="capacidad" name="capacidad">
              </div>

              <br>

              <div class="form-group">
                <label for="capacidad">Cilindraje</label>
                <input required type="text" class="form-control" id="capacidad" name="cilindraje">
              </div>

              <br>

              <div class="input-group mb-3">
                <label for="capacidad">foto</label>
                <br>
                  <input required type="file" class="form-control" placeholder="Foto" name="foto">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-user"></span>
                    </div>
                  </div>
              </div>

              <br>

               <div class="form-group">
                <label for="empresa">Licencia</label>
                <select required class="form-control" data-style="btn btn-link" id="empresa" name="licencia_id">
                  <option value="" selected disabled>Seleccione...</option>

                  @foreach ($rowsl as $row)
                  <option value="{{$row->id}}">{{$row->tipo}}</option>
                  @endforeach

                </select>
              </div>

              <br>


              <div class="form-group">
                <label for="operacion">Estado Operativo</label>
                 <select required class="form-control" data-style="btn btn-link" id="operacion" name="operacion_estado">
                  <option value="" selected disabled>Seleccione...</option>
                  <option value="Activo">Activo</option>
                  <option value="Inactivo">Inactivo</option>
                </select>
              </div>

              @if(Auth::user()->role_id==1)

               <div class="form-group">
                <label for="licencia">Estado de la licencia</label>
                 <select required class="form-control" data-style="btn btn-link" id="licencia" name="licencia_estado">
                  <option value="" selected disabled>Seleccione...</option>
                  <option value="Activa">Activa</option>
                  <option value="Inactiva">Inactiva</option>
                </select>
              </div>

              <br>

               <div class="form-group">
                <label for="empresa">Empresa</label>
                <select required class="form-control" data-style="btn btn-link" id="empresa" name="empresa_id">
                  <option value="" selected disabled>Seleccione...</option>

                  @foreach ($rows as $row)
                  <option value="{{$row->id}}">{{$row->nombre}}</option>
                  @endforeach

                </select>
              </div>
              @endif
              
              <br>

              <button type="submit" class="btn btn-success">Submit</button>
              <a href="{{url('autobuses')}}" class="btn btn-info float-right">Volver</a>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection