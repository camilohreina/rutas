@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Typography')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Autobuses</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="autobuses">
          <div class="card-title">
            <h2>Buses</h2>
            @if (Auth::user()->role_id==1 || Auth::user()->role_id==2)
            <a href="{{url('autobuses/create')}}" class="btn btn-info float-right">Crear</a>
            @endif
            <br>
            <br>
          </div>

         <div class="row col-md-12">
            @foreach ($rows as $row)
                
               
                    <div class="card mr-4" style="width: 20rem;">
                      <div class="card-body">
                        <h4 class="card-title" style="color: black!important"><strong>{{$row->placa}}</strong></h4>
                        <hr>
                        <img class="card-img-top mb-2" style="height: 180px" src="{{ url('public/images/'.$row->foto)  }}">
                        <!-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> -->
                        <p class="card-text" style="margin-bottom: 0.7em!important">Capacidad: {{$row->capacidad}}</p>
                        <p class="card-text" style="margin-bottom: 0.7em!important">Cilindraje: {{$row->cilindraje}}</p>
                        <p class="card-text" style="margin-bottom: 0.7em!important">Licencia: {{$row->licencia->tipo}}</p>
                        <p class="card-text" style="margin-bottom: 0.7em!important">Estado de Licencia: {{$row->licencia_estado}}</p>
                        <p class="card-text" style="margin-bottom: 0.7em!important">Estado de Operación: {{$row->operacion_estado}}</p>
                         @if (Auth::user()->role_id==1)
                        <p class="card-text" style="margin-bottom: 0.7em!important">Empresa: {{$row->empresa->nombre}}</p>
                        @endif

                      @if (Auth::user()->role_id==1 || Auth::user()->role_id==2)
                      <div class="row">
                       <a href="{{url('licencias')}}" style="color: white" rel="tooltip" class="btn btn-info mr-2">
                                      <i class="material-icons">attach_money</i>
                       </a>
                      <a href="{{ route('autobuses.edit', $row->id) }}" rel="tooltip" class="btn btn-success mr-2" style="color: white">
                                    <i class="material-icons">edit</i>
                      </a>
                      <form  action="{{url('autobuses/'.$row->id)}}" method="post">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" rel="tooltip" class="btn btn-danger">
                                      <i class="material-icons">close</i>
                                    </button>
                      </form>
                      </div>
                      @endif
                      </div>
                    </div>
                
                
            @endforeach
    </div>   


          
        </div>
      </div>
    </div>
  </div>
</div>
@endsection