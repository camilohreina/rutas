@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Empresa')])
@section('scripts')
<script src="{{url('public/pages/crear.js')}}" type="text/javascript"></script>
@endsection
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Actualizar empresa</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="empresas" class="container w-50">
              <form action=" {{url('empresas/'.$data->id)}} " method="POST" autocomplete="off" id="form-general">
                @csrf
                <input type="hidden" name="_method" value="PATCH">
                <div class="box-body m-4">
                    @include('empresas.form')
                    @include('includes.boton-form-editar', ['url'=>'empresas'])
                </div>
              </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection