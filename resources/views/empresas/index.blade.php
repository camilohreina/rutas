@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Empresa')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Empresas registradas</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="autobuses">
          <div class="card-title">
            <a href=" {{url('empresas/create')}} " rel="tooltip" class="btn btn-info btn-sm float-right ml-2 mr-2">
              Agregar empresa
            </a>
            <table class="table">
              <thead>
                  <tr>
                      <th class="text-center">#</th>
                      <th>Nombre</th>
                      <th>Nit</th>
                      <th class="text-right">Acciones</th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($datas as $key => $data)
                <tr>
                  <td class="text-center"> {{$key + 1}} </td>
                  <td> {{$data->nombre}} </td>
                  <td> {{$data->nit}} </td>
                  <td class="td-actions text-right">
                      {{-- <button type="button" rel="tooltip" class="btn btn-info ml-2 mr-2">
                          <i class="material-icons">person</i>
                      </button> --}}
                      <a href=" {{url('empresas/'.$data->id.'/edit')}} " rel="tooltip" class="btn btn-success ml-2 mr-2">
                          <i class="material-icons">edit</i>
                      </a>
                      <form class="d-inline" action=" {{url('empresas/'.$data->id)}} " method="POST" autocomplete="off">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" rel="tooltip" class="btn btn-danger">
                          <i class="material-icons">close</i>
                      </button>
                      </form>
                  </td>
              </tr>
                @endforeach
                  
              </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection