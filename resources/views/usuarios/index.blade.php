@extends('layouts.app', ['activePage' => 'typography', 'titlePage' => __('Typography')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Nuestros usuarios</h4>
        <p class="card-category"></p>
      </div>
      <div class="card-body">
        <div id="autobuses">
          <div class="card-title">
            <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Empresa</th>
                            <th>Registrado</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $key => $data)
                        <tr>
                            <td> {{$data->nombre}} </td>
                            <td> {{$data->apellido}} </td>
                            <td> {{$data->email}} </td>
                            <td> {{ ucfirst($data->role->nombre) }} </td>
                            <td> {{ ucfirst($data->empresa->nombre) }} </td>
                            <td> {{ date('d-H-y, h:i a', strtotime($data->created_at)) }}  </td>
                            <td class="td-actions text-right">
                              <a href=" {{url('perfil/'.$data->id)}} " rel="tooltip" class="btn btn-info ">
                                <i class="material-icons">person</i>
                              </a>
                              <a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
                                <i class="material-icons">edit</i>
                              </a>
                              </button>
                              <button type="button" rel="tooltip" class="btn btn-danger ml-2 mr-2">
                                  <i class="material-icons">close</i>
                              </button>
                          </td>
                        </tr>   
                        @endforeach
                        
                    </tbody>
                </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cambiar role</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow: hidden;">
        <form action=" {{url('cambiar-empresa')}} " method="post" class="d-inline" autocomplete="off">
        <input type="hidden" name="_method" value="PATCH">
        @csrf
        <input type="hidden" name="id" value=" {{$data->id}} ">
        <div class="mb-2">
          <select class="form-control" name="role" width="100%" required>
            <option disabled selected>Seleccione el role para este usuario...</option>
            @foreach ($roles as $role)
              @if ($role->nombre_visible != 'Super Administrador')  
              <option value=" {{$role->id}} "> {{ ucfirst($role->nombre_visible)}} </option>
              @endif
            @endforeach
          </select>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Cambiar</button>
        </div>
      </form>
      </div>
  </div>
</div>
@endsection


