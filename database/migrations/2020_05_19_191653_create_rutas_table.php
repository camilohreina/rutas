<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRutasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rutas', function (Blueprint $table) {
            $table->id();

            $table->integer('empresa_id');
            $table->string('nombre');
            $table->string('origen');
            $table->string('destino');
            $table->string('latitud_origen');
            $table->string('longitud_origen');
            $table->string('latitud_destino');
            $table->string('longitud_destino');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rutas');
    }
}
