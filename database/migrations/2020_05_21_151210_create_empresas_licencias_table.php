<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasLicenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas_licencias', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('empresa_id')->nullable();
            $table->unsignedBigInteger('licencia_id')->nullable();
            $table->integer('licencias_compradas')->nullable();
            $table->integer('licencias_disponibles')->nullable();

            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->foreign('licencia_id')->references('id')->on('licencias');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas_licencias');
    }
}
